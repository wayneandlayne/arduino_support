WNL Readme
==========
Usage
-----

Add https://bitbucket.org/wayneandlayne/arduino_support/raw/master/artifacts/package_wayneandlayne_index.json to the boards manager in arduino.  If you already have something there, add it with a comma.

You will need the SAMD boards.

Adding a new update
-------------------
Make a copy of the tree, delete .git and artifacts, tar cjf it as wayneandlayne_x.x.x.tar.bz2, ls -l to get the size and shasum -a256 to get the shasum, edit the index.json to add a new entry, and copy the tar bz2 into artifacts.  push and merge into master.

$ cp -r arduino_support wayneandlayne_1.0.0; rm -rf wayneandlayne_1.0.0/artifacts wayneandlayne_1.0.0/.git; tar cjf arduino_support/artifacts/wayneandlayne_1.0.0.tar.bz2 wayneandlayne_1.0.0; shasum -a 256 arduino_support/artifacts/wayneandlayne_1.0.0.tar.bz2 ; ls -l arduino_support/artifacts/wayneandlayne_1.0.0.tar.bz2
